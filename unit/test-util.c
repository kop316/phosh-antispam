/*
 *
 *  Visual Voicemail Daemon
 *
 *  Copyright (C) 2022, Chris Talbot <chris@talbothome.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include <glib.h>
#include <glib/gprintf.h>

#include "aspam-settings.h"


static const char *const match_list_empty_1[] = {
  "",
  "555",
  "",
  "444",
  "",
  NULL,
};

static const char *const match_list_empty_2[] = {
  "555",
  "444",
  "333",
  NULL,
};


static void
test_util (gconstpointer data)
{
  g_autofree char *csv = NULL;

  csv = aspam_settings_make_csv_match_list ((char **) match_list_empty_1);
  g_assert_cmpstr (csv, ==, "555,444");
  g_clear_pointer (&csv, g_free);

  csv = aspam_settings_make_csv_match_list ((char **) match_list_empty_2);
  g_assert_cmpstr (csv, ==, "555,444,333");
  g_clear_pointer (&csv, g_free);
}

int
main (int    argc,
      char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_data_func ("/vvmutil/Util Test",
                        NULL, test_util);


  return g_test_run ();
}
